import React, { Component } from 'react';
import axios from 'axios';

import PageHeader from '../template/pageHeader'
import TodoForm from './todoForm'
import TodoList from './todoList'
import todoForm from './todoForm';

const URL = 'http://localhost:3003/api/todos'
class Todo extends Component {

    constructor(props) {
        super(props)
        this.state = { descricao: '', list: [] }

        this.handleAdd = this.handleAdd.bind(this)
        this.handleChange = this.handleChange.bind(this)
        this.handleRemove = this.handleRemove.bind(this)
        this.handleMarkAsDone = this.handleMarkAsDone.bind(this)
        this.handleMarkAsPending = this.handleMarkAsPending.bind(this)
        this.handleSearch = this.handleSearch.bind(this)

        this.refresh()
    }

    refresh(descricao = '') {
        const search = descricao ? `&descricao__regex=/${descricao}/` : ''
        axios.get(`${URL}?sort=-dtCriacao${search}`).
        then(resp => this.setState({...this.state, descricao, list: resp.data}))
    }

    handleMarkAsDone(todo) {
        debugger
        axios.put(`${URL}/${todo._id}`, { ...todo, concluida: true})
        .then(resp => this.refresh(this.state.descricao))
    }

    handleSearch() {
        this.refresh(this.state.descricao)
    }

    handleMarkAsPending(todo) {
        axios.put(`${URL}/${todo._id}`, { ...todo, concluida: false})
        .then(resp => this.refresh(this.state.descricao))
    }

    handleRemove(todo) {
        axios.delete(`${URL}/${todo._id}`).then(resp => this.refresh(this.state.descricao))
    }

    handleChange(e) {
        this.setState({
            ...this.state, descricao: e.target.value 
        })
    }

    handleAdd() {
        const descricao = this.state.descricao
        axios.post(URL, { descricao }).then(resp => this.refresh())
    }

    render() { 
        return ( 
            <div>
                <PageHeader name='Tarefas' small='Cadastro' />
                <TodoForm descricao={this.state.descricao} 
                            handleAdd={this.handleAdd} 
                            handleChange={this.handleChange}
                            handleSearch={this.handleSearch}/>
                <TodoList list={this.state.list} handleRemove={this.handleRemove}
                        handleMarkAsDone={this.handleMarkAsDone}
                        handleMarkAsPending={this.handleMarkAsPending}/>
            </div>
         );
    }
}
 
export default Todo;